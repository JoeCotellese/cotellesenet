---
title: "Leaving Facebook"
date: 2019-04-29T10:25:50-04:00
draft: false
---

I'm leaving Facebook. I realized over the last few years that the value I recieved using Facebook wasn't worth the privacy that I am giving up.

I'm still available online, I'm just going to post my content on my own personal sites, this one, for random stuff that I'd typically talk about on Facebook and JoeCotellese.com for my professional writing. 

You can also still find me on Twitter and LinkedIn.

I'm going to keep my Facebook profile open for a little while but won't be checking it. 

## Why I'm leaving?

I'm just not getting value from Facebook. Initially, I joined Facebook when I was working at another social network. It was fun connecting with friends, old schoolmates and family. The novelty eventually wore off and I found that I was using it less and less. When I did it was mostly to complain about politics. 

## Related Links

Facebook has played a negative roll in our election, as well as Brexit. This is an excellent Ted Talk with Carole Caldwalladr about Facebook's role in democracy.

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/carole_cadwalladr_facebook_s_role_in_brexit_and_the_threat_to_democracy" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>





